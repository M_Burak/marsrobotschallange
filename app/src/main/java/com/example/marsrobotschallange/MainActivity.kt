package com.example.marsrobotschallange

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.marsrobotschallange.viewModel.MainActivityViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var input = listOf<String>()
    private var gridCoordinates = ""
    private lateinit var viewModel: MainActivityViewModel
    var outputString = ""
    private val outputListDataObserver = Observer<MutableList<String>> { list ->

        list.forEach{
            outputString += "$it---"
        }

        tvOutput.text = outputString
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        viewModel.observableOutput.observe(this, outputListDataObserver)
        btnGo.setOnClickListener {
            outputString = ""
            tvOutput.text=""
            validateInput()
        }
    }

    // A lot of other validations can be made such as which letters are used, or if the number of lines are correct or not
    // But i skipped them due to the fact that this is not the main point of the application

    private fun validateInput() {
        var instructionRawList = mutableListOf<String>()

        //Take the whole input
        input = instructions.text.lines()

        //First line is the grid coordinates
        gridCoordinates = input.first()

        // Here we have a list of lists, each list has 2 items corresponding to the position and
        // instructions for each robot, first item of input removed because it is for grid coordinates
        for (i in 1 until input.size) {
            instructionRawList.add(input[i])
        }

        var instructionList = instructionRawList.chunked(2)

        val inputValidation = input.first().toString().split(" ")

        var errorFree = true

        inputValidation.apply {
            if (first().toInt() > 50 || inputValidation[1].toInt() > 50) {
                showError("Grid coordinates can't be bigger than 50!")
            } else {
                instructionList.forEach {
                    if (it[1].length >= 100) {
                        errorFree = false
                        showError("All instruction strings should be less than 100 letters")
                    }
                }
            }
        }

        if (errorFree) viewModel.processInput(instructionList, gridCoordinates)

    }

    private fun showError(errorMessage: String) {
        Toast.makeText(
            this, errorMessage,
            Toast.LENGTH_LONG
        ).show()
    }
}
