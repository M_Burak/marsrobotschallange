package com.example.marsrobotschallange.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    private var scentedPositions: MutableMap<String, String> = mutableMapOf()
    private var output = mutableListOf<String>()
    private var gridX = 0
    private var gridY = 0
    private var isRobotLost = false
    var observableOutput = MutableLiveData<MutableList<String>>()

    private fun calculateNextOrientation(orientation: String, instruction: String): String {

        with(orientation) {
            if (this == "N") {
                return if (instruction == "R")
                    "E"
                else "W"
            } else {
                if (this == "W") {
                    return if (instruction == "R")
                        "N"
                    else "S"
                } else {
                    if (this == "S") {
                        return if (instruction == "R")
                            "W"
                        else "E"
                    } else {
                        if (this == "E") {
                            return if (instruction == "R")
                                "S"
                            else "N"
                        }

                    }
                }
            }
        }
        return ""
    }

    private fun calculateNextPosition(x: String, y: String, orientation: String): List<String> {
        var xInt = x.toInt()
        var yInt = y.toInt()
        var isPositionScented = isScented(xInt, yInt)

        with(orientation) {
            if (this == "E") {
                // if robots current x position equals to the final x position we need to check if that point is scented
                if (xInt == gridX) {
                    // If the position is scented and it is scented in the directory of current orientation
                    // don't process and return the same x,y values
                    if (!isPositionScented.isNullOrBlank()) {
                        if (this == isPositionScented) return listOf(x, y)
                        else {
                            //Scent position if it is the final x position of grid and mark robot as lost
                            scentedPositions[x + y] = orientation
                            isRobotLost = true
                        }
                    }
                }

                xInt++
            }
            if (this == "W") {
                // if robots current x position equals to the first -0- x position we need to check if that point is scented
                if (xInt == 0) {
                    // If the position is scented and it is scented in the directory of current orientation
                    // don't process and return the same x,y values
                    if (!isPositionScented.isNullOrBlank()) {
                        if (this == isPositionScented) return listOf(x, y)
                        else {
                            //Scent position if it is the first x position of grid and mark robot as lost
                            scentedPositions[x + y] = orientation
                            isRobotLost = true
                        }
                    }
                }
                xInt--
            }
            if (this == "S") {
                // if robots current x position equals to the first -0- x position we need to check if that point is scented
                if (yInt == 0) {
                    // If the position is scented and it is scented in the directory of current orientation
                    // don't process and return the same x,y values
                    if (!isPositionScented.isNullOrBlank()) {
                        if (this == isPositionScented) return listOf(x, y)
                        else {
                            //Scent position if it is the first y position of grid and mark robot as lost
                            scentedPositions[x + y] = orientation
                            isRobotLost = true
                        }
                    }
                }
                yInt--
            }
            if (this == "N") {
                // if robots current y position equals to the final y position we need to check if that point is scented
                if (yInt == gridY) {
                    // If the position is scented and it is scented in the directory of current orientation
                    // don't process and return the same x,y values
                    if (!isPositionScented.isNullOrBlank()) {
                        if (this == isPositionScented) return listOf(x, y)
                        else {
                            //Scent position if it is the final y position of grid and mark robot as lost
                            scentedPositions[x + y] = orientation
                            isRobotLost = true
                        }
                    }
                }
                yInt++
            }
        }

        return listOf(xInt.toString(), yInt.toString())
    }

    //If position is scented return the orientation that a robot can't go to, else return empty string
    private fun isScented(x: Int, y: Int): String? {
        return scentedPositions[x.toString() + y.toString()]
    }


    private fun calculateFinalPosition(item: List<String>) {
        //get x,y and orientation of the item
        var positionInfo = item.first().split(" ")
        var x = positionInfo.first()
        var y = positionInfo[1]
        var orientation = positionInfo[2]

        //process the instruction string

        var instructionString = item[1]
        for (i in instructionString.indices) {
            // if instruction is R or L calculate next orientation
            if ("RL".contains(instructionString[i])) {
                orientation = calculateNextOrientation(
                    orientation,
                    instructionString[i].toString()
                )
            } else {
                // if instruction is F calculate next position
                if (instructionString[i].toString() == "F") {
                    var positions = calculateNextPosition(x, y, orientation)
                    x = positions.first()
                    y = positions[1]
                }
            }
        }

        var finalPosition = if (isRobotLost) "$x $y $orientation LOST"
        else "$x $y $orientation"

        output.add(finalPosition)
    }

    fun processInput(list: List<List<String>>, gridCoordinates: String) {
        observableOutput.value?.clear()
        scentedPositions.clear()
        var gridPositionList = gridCoordinates.split(" ")
        gridX = gridPositionList.first().toInt()
        gridY = gridPositionList[1].toInt()

        //Process each robot sequentially
        list.forEach {
            isRobotLost = false
            calculateFinalPosition(it)
        }

        observableOutput.value = output

    }
}